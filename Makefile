.SUFFIXES:.out .o .s .c .F .f .f90 .e .r .y .yr .ye .l .p .sh .csh .h

FLAGS = -c -O  


OBJECTS = \
create_pecube_in.o \
find_element.o \
find_dt.o \
find_neighbours.o \
find_temp.o \
find_velo.o \
geometry.o \
interpol3d.o \
interpolate.o \
make_matrix.o \
muscovite.o \
pecubeages.o \
solve_iterative.o \
solve_conjugate_gradient.o \
tridag.o \
velocheck.o\
zirconHe.o \
Pecube.o

.f90.o:
	ifort $(INCLUDE) $(FLAGS) $*.f90

.f.o:
	ifort $(INCLUDE) $(FLAGS) $*.f

.c.o:
	gcc $(INCLUDE) $(FLAGS) $*.c

Pecube:	$(OBJECTS)
	ifort -O $(OBJECTS) $(LIB) -o Pecube
