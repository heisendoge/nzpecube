      subroutine pecubeages (xmisfit,xsurf,ysurf,zsurf,nsurf)

!   implicit real*8 (a-h,o-z) 
      
      real*8::                            xsurf(nsurf),ysurf(nsurf),zsurf(nsurf)
      real*4,dimension(:,:),allocatable:: pressure,temperature,xloc,yloc
      real*4,dimension(:,:),allocatable:: height,xn,yn,zn,zhe_age,musc_age,temp_max
      real*4,dimension(:),allocatable::   age,age_int,age_int2,age_mes,ages,xv,yv,av,xer
      real*4::                            lenght(22),zage,mlt,d0,xmisfit
      integer nx,ny,npp,np,nage,nsurf,is

      real xr(5),yr(5),closure(2),ftld(17)
      
       common /nxny/ nx,ny

       if (nx*ny.ne.nsurf) then
       print*,'ny*ny not equal to nsurf in pecubeages'
       STOP
       endif

! we reorganize the grid for element search
       allocate(xn(nx,ny),yn(nx,ny),zn(nx,ny))
       is=0
       do j=1,ny
        do i=1,nx
  	 is=is+1
  	 xn(i,j)=xsurf(is)
  	 yn(i,j)=ysurf(is)
       	 zn(i,j)=zsurf(is)
        enddo
       enddo

! we read Pecube.ptt
!      open (10,file='RUN01/Pecube.ptt',status='old',access='direct',recl=4*(1+4*nsurf))

      nage=1
    3 read (10,rec=nage,err=4) aaa
      if (aaa.lt.0.) goto 4
      nage=nage+1
      goto 3
    4 allocate (age(nage),pressure(nage,nsurf),temperature(nage,nsurf),xloc(nage,nsurf),yloc(nage,nsurf))
      

        do i=1,nage  
        read (10,rec=i)   age(i),(temperature(i,k),k=1,nsurf),(pressure(i,k),k=1,nsurf), &
                       (xloc(i,k),k=1,nsurf),(yloc(i,k),k=1,nsurf)
        enddo
!        age(nage)=age(nage-1)/100.
        age(1)=100. !the non-reset ages are 100 Ma

! here we calculate the ages of all the samples at the surface

      print*,'we do muscovite ages'
      allocate (musc_age(nx,ny))
      do j=1,ny
	 do i=1,nx
         call muscovite (age,temperature(:,(j-1)*nx+i),nage,musc_age(i,j))
         enddo
      enddo
      print*,'minage=',minval(musc_age),'maxage=',maxval(musc_age) 

      print*,'we do zircon He ages'
      allocate (zhe_age(nx,ny))
      do j=1,ny
         do i=1,nx
           call zirc_he (age,temperature(:,(j-1)*nx+i),nage,zhe_age(i,j)) 
         enddo
      enddo
      print*,'minage=',minval(zhe_age),'maxage=',maxval(zhe_age)

!get max temperature for Raman data

       print*,'we do Raman temp'
       allocate (temp_max(nx,ny))
       temp_max=0.
        do j=1,ny
         do i=1,nx
          do k=1,nage
           if (temperature(k,(j-1)*nx+i).gt.temp_max(i,j)) temp_max(i,j)=temperature(k,(j-1)*nx+i)
          enddo
         enddo
        enddo
       print*,'min t_max=',minval(temp_max),'max t_max=',maxval(temp_max)
    
 
! create ouput for ages map
   open (98,file='Pecubeout.txt',status='unknown')
   write (98,*) nx,ny
   do j=1,ny
    do i=1,nx
     write (98,*) xn(i,j),yn(i,j),zn(i,j),musc_age(i,j),temp_max(i,j),zhe_age(i,j)
    enddo
   enddo
  close(98)

! calculate misfit
!    call Afit(xn,yn,nx,ny,musc_age,xv,yv,av,xer,npp,amisfit)
! deallocate all the files
    deallocate(zhe_age,musc_age,age,pressure,temperature,xloc,yloc,xn,yn)
    
! returns total misfit value	  
     xmisfit=amisfit
     return
     end
