      subroutine solve_iterative (n,ndof,ael,f,x,kfix,icon, &
                                  nnode,nelem,niter)

      implicit real*8 (a-h,o-z)

! Gauss-Seidel itertive solver

! note that this routine has only been checked for ndof=1

      real*8 ael(n*ndof,n*ndof,nelem),f(ndof*nnode),x(ndof*nnode)
      integer icon(n,nelem),kfix(nnode*ndof)

      integer,dimension(:),allocatable::jcmax
      integer,dimension(:,:),allocatable::jc,kc
      real*8,dimension(:),allocatable::diag,x0

! prepare inverse connectivity arrays

      allocate (jcmax(nnode),diag(nnode*ndof),x0(nnode*ndof))

      jcmax=0
        do ielem=1,nelem
          do inode=1,n
          ic=icon(inode,ielem)
          jcmax(ic)=jcmax(ic)+1
          enddo
        enddo

      melem=maxval(jcmax)

      allocate (jc(melem,nnode),kc(melem,nnode))

      jcmax=0
        do ielem=1,nelem
          do inode=1,n
          ic=icon(inode,ielem)
          jcmax(ic)=jcmax(ic)+1
          jc(jcmax(ic),ic)=ielem
          kc(jcmax(ic),ic)=inode
          enddo
        enddo

! prepare diagonal

      diag=0.
        do ie=1,nelem
          do k=1,n
          ic=icon(k,ie)
          diag(ic)=diag(ic)+ael(k,k,ie)
          enddo
        enddo

! start of iterations

      itmax=100000
      tol=1.e-6
      beta=1.3
      x0=x

      it=0
1     it=it+1
      if (it.gt.itmax) stop 'too many iterations'
        do inode=1,nnode
          sup=f(inode)
          do jelem=1,jcmax(inode)
          ielem=jc(jelem,inode)
          knode=kc(jelem,inode)
            do jnode=1,n
            ic=icon(jnode,ielem)
            sup=sup-ael(knode,jnode,ielem)*x(ic)
            enddo
          enddo
        if (kfix(inode).ne.1) x(inode)=x0(inode)+beta*sup/diag(inode)
        enddo

! check for convergence

      sum=0.
      sum0=0.
        do inode=1,nnode
!        sum=sum+x(inode)**2
!        sum0=sum0+(x(inode)-x0(inode))**2
        sum=max(sum,abs(x(inode)))
        sum0=max(sum0,abs((x(inode)-x0(inode))))
        enddo

      x0=x

      if(sum0.gt.sum*tol) goto 1

      deallocate (jcmax,jc,kc,diag,x0)

      niter=it

      return
     end
