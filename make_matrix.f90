      subroutine make_matrix (mpe,ael,bel,icon,xg,yg,zg,xgp,ygp,zgp, &
                              kfix,diffusivity,heatproduction,Peclet, &
                              alpha0,dt0,time,tempp,nnode,istatic,zl, &
                              xming,xmaxg,yming,ymaxg,stress,hpt, &
                              arctrench,ddepth,dip,acr,tacr,Rbuf,az)   !SK

      implicit real*8 (a-h,o-z)

! this is where the FE matrices and RHS vectors are built

      real*8 xg(nnode),yg(nnode),zg(nnode)
      real*8 xgp(nnode),ygp(nnode),zgp(nnode)
      real*8 tempp(nnode)
      real*8 ael(mpe,mpe),bel(mpe)
      real*8 jcb(3,3),jcbi(3,3),jcbp(3,3),jcbip(3,3)
      integer icon(mpe),kfix(nnode)
      real*8,dimension(:),allocatable :: rr,ss,tt,ww
      real*8,dimension(:,:),allocatable :: aelp,b,bp,vv
      real*8,dimension(:),allocatable :: x,y,z,xp,yp,zp,h,ht,dhdr,dhds,dhdt

      common /crust/ zc

      allocate (aelp(mpe,mpe),b(mpe,3),bp(mpe,3))
      allocate (x(mpe),y(mpe),z(mpe),xp(mpe),yp(mpe),zp(mpe))
      allocate (vv(mpe,3))
      allocate (h(mpe),ht(mpe),dhdr(mpe),dhds(mpe),dhdt(mpe))

      eps=tiny(eps)
      pi=atan(1.)*4.

        if (mpe.eq.8) then
        nint=8
        else
        nint=6
        endif

      rhoc=1.
      g=9.81
      rho=2700.
      c=1000.
      fric=1.    !0= no shear heat, 1 = shear heat on.
      cond=diffusivity
      heat=heatproduction
      velo=Peclet
      dynamic=1.
      dt=dt0
      alpha=alpha0
      zmax=maxval(zg)
        if (istatic.eq.1) then
        dynamic=0.
        dt=1.
        alpha=1.
        endif

      allocate (rr(nint),ss(nint),tt(nint),ww(nint))

      if (nint.eq.6) then
      rr(1)=0.16667
      ss(1)=0.16667
      tt(1)=-.57735
      ww(1)=1.
      rr(2)=0.66667
      ss(2)=0.16667
      tt(2)=-.57735
      ww(2)=1.
      rr(3)=0.16667
      ss(3)=0.66667
      tt(3)=-.57735
      ww(3)=1.
      rr(4)=0.16667
      ss(4)=0.16667
      tt(4)=.57735
      ww(4)=1.
      rr(5)=0.66667
      ss(5)=0.16667
      tt(5)=.57735
      ww(5)=1.
      rr(6)=0.16667
      ss(6)=0.66667
      tt(6)=.57735
      ww(6)=1.
      elseif (nint.eq.8) then
      rr(1)=-.57735
      ss(1)=-.57735
      tt(1)=-.57735
      ww(1)=1.
      rr(2)=.57735
      ss(2)=-.57735
      tt(2)=-.57735
      ww(2)=1.
      rr(3)=.57735
      ss(3)=.57735
      tt(3)=-.57735
      ww(3)=1.
      rr(4)=-.57735
      ss(4)=.57735
      tt(4)=-.57735
      ww(4)=1.
      rr(5)=-.57735
      ss(5)=-.57735
      tt(5)=.57735
      ww(5)=1.
      rr(6)=.57735
      ss(6)=-.57735
      tt(6)=.57735
      ww(6)=1.
      rr(7)=.57735
      ss(7)=.57735
      tt(7)=.57735
      ww(7)=1.
      rr(8)=-.57735
      ss(8)=.57735
      tt(8)=.57735
      ww(8)=1.
      endif

        do k=1,mpe
        x(k)=xg(icon(k))
        y(k)=yg(icon(k))
        z(k)=zg(icon(k))
        xp(k)=xgp(icon(k))
        yp(k)=ygp(icon(k))
        zp(k)=zgp(icon(k))
        enddo

      ael=0.
      aelp=0.
      bel=0.

      timep=time-dt

        do i=1,mpe
		call find_velo (x(i),y(i),z(i),dt,velo,time, &
		                xming,xmaxg,yming,ymaxg,zl,vv(i,1),vv(i,2),vv(i,3), &
                                arctrench,ddepth,dip,acr,tacr,Rbuf,az)  !SK
        enddo
	  vv=vv*velo

        do iint=1,nint
        r=rr(iint)
        s=ss(iint)
        t=tt(iint)
        w=ww(iint)

          if (mpe.eq.8) then
          h(1)=(1.-r)*(1.-s)*(1.-t)/8.
          h(2)=(1.+r)*(1.-s)*(1.-t)/8.
          h(3)=(1.+r)*(1.+s)*(1.-t)/8.
          h(4)=(1.-r)*(1.+s)*(1.-t)/8.
          h(5)=(1.-r)*(1.-s)*(1.+t)/8.
          h(6)=(1.+r)*(1.-s)*(1.+t)/8.
          h(7)=(1.+r)*(1.+s)*(1.+t)/8.
          h(8)=(1.-r)*(1.+s)*(1.+t)/8.
          else
          h(1)=(1.-r-s)*(1.-t)/2.
          h(2)=r*(1.-t)/2.
          h(3)=s*(1.-t)/2.
          h(4)=(1.-r-s)*(1.+t)/2.
          h(5)=r*(1.+t)/2.
          h(6)=s*(1.+t)/2.
          endif

        xint=0.
        yint=0.
        zint=0.
          do k=1,mpe
          xint=xint+h(k)*x(k)
          yint=yint+h(k)*y(k)
          zint=zint+h(k)*z(k)
          enddo

          if (mpe.eq.8) then
          dhdr(1)=-(1.-s)*(1.-t)/8.
          dhdr(2)=(1.-s)*(1.-t)/8.
          dhdr(3)=(1.+s)*(1.-t)/8.
          dhdr(4)=-(1.+s)*(1.-t)/8.
          dhdr(5)=-(1.-s)*(1.+t)/8.
          dhdr(6)=(1.-s)*(1.+t)/8.
          dhdr(7)=(1.+s)*(1.+t)/8.
          dhdr(8)=-(1.+s)*(1.+t)/8.
          dhds(1)=-(1.-r)*(1.-t)/8.
          dhds(2)=-(1.+r)*(1.-t)/8.
          dhds(3)=(1.+r)*(1.-t)/8.
          dhds(4)=(1.-r)*(1.-t)/8.
          dhds(5)=-(1.-r)*(1.+t)/8.
          dhds(6)=-(1.+r)*(1.+t)/8.
          dhds(7)=(1.+r)*(1.+t)/8.
          dhds(8)=(1.-r)*(1.+t)/8.
          dhdt(1)=-(1.-r)*(1.-s)/8.
          dhdt(2)=-(1.+r)*(1.-s)/8.
          dhdt(3)=-(1.+r)*(1.+s)/8.
          dhdt(4)=-(1.-r)*(1.+s)/8.
          dhdt(5)=(1.-r)*(1.-s)/8.
          dhdt(6)=(1.+r)*(1.-s)/8.
          dhdt(7)=(1.+r)*(1.+s)/8.
          dhdt(8)=(1.-r)*(1.+s)/8.
          else
          dhdr(1)=-(1.-t)/2.
          dhdr(2)=(1.-t)/2.
          dhdr(3)=0.
          dhdr(4)=-(1.+t)/2.
          dhdr(5)=(1.+t)/2.
          dhdr(6)=0.
          dhds(1)=-(1.-t)/2.
          dhds(2)=0.
          dhds(3)=(1.-t)/2.
          dhds(4)=-(1.+t)/2.
          dhds(5)=0.
          dhds(6)=(1.+t)/2.
          dhdt(1)=-(1.-r-s)/2.
          dhdt(2)=-r/2.
          dhdt(3)=-s/2.
          dhdt(4)=(1.-r-s)/2.
          dhdt(5)=r/2.
          dhdt(6)=s/2.
          endif

        jcb=0.
        jcbp=0.
          do k=1,mpe
          jcb(1,1)=jcb(1,1)+dhdr(k)*x(k)
          jcb(1,2)=jcb(1,2)+dhdr(k)*y(k)
          jcb(1,3)=jcb(1,3)+dhdr(k)*z(k)
          jcb(2,1)=jcb(2,1)+dhds(k)*x(k)
          jcb(2,2)=jcb(2,2)+dhds(k)*y(k)
          jcb(2,3)=jcb(2,3)+dhds(k)*z(k)
          jcb(3,1)=jcb(3,1)+dhdt(k)*x(k)
          jcb(3,2)=jcb(3,2)+dhdt(k)*y(k)
          jcb(3,3)=jcb(3,3)+dhdt(k)*z(k)
          jcbp(1,1)=jcbp(1,1)+dhdr(k)*xp(k)
          jcbp(1,2)=jcbp(1,2)+dhdr(k)*yp(k)
          jcbp(1,3)=jcbp(1,3)+dhdr(k)*zp(k)
          jcbp(2,1)=jcbp(2,1)+dhds(k)*xp(k)
          jcbp(2,2)=jcbp(2,2)+dhds(k)*yp(k)
          jcbp(2,3)=jcbp(2,3)+dhds(k)*zp(k)
          jcbp(3,1)=jcbp(3,1)+dhdt(k)*xp(k)
          jcbp(3,2)=jcbp(3,2)+dhdt(k)*yp(k)
          jcbp(3,3)=jcbp(3,3)+dhdt(k)*zp(k)
          enddo

        volume=jcb(1,1)*jcb(2,2)*jcb(3,3)+jcb(1,2)*jcb(2,3)*jcb(3,1) &
              +jcb(2,1)*jcb(3,2)*jcb(1,3) &
              -jcb(1,3)*jcb(2,2)*jcb(3,1)-jcb(1,2)*jcb(2,1)*jcb(3,3) &
              -jcb(2,3)*jcb(3,2)*jcb(1,1)
        volumep=jcbp(1,1)*jcbp(2,2)*jcbp(3,3)+jcbp(1,2)*jcbp(2,3)*jcbp(3,1) &
               +jcbp(2,1)*jcbp(3,2)*jcbp(1,3) &
               -jcbp(1,3)*jcbp(2,2)*jcbp(3,1)-jcbp(1,2)*jcbp(2,1)*jcbp(3,3) &
               -jcbp(2,3)*jcbp(3,2)*jcbp(1,1)

          if (volume.le.eps) then
          print*,'volume= ',volume
          pause
          stop 'Element bow-tied or collapsed'
          endif

          if (volumep.le.eps) then
          print*,'volumep= ',volumep
          pause
          stop 'Element bow-tied or collapsed'
          endif

        jcbi(1,1)=(jcb(2,2)*jcb(3,3)-jcb(2,3)*jcb(3,2))/volume
        jcbi(2,1)=(jcb(2,3)*jcb(3,1)-jcb(2,1)*jcb(3,3))/volume
        jcbi(3,1)=(jcb(2,1)*jcb(3,2)-jcb(2,2)*jcb(3,1))/volume
        jcbi(1,2)=(jcb(1,3)*jcb(3,2)-jcb(1,2)*jcb(3,3))/volume
        jcbi(2,2)=(jcb(1,1)*jcb(3,3)-jcb(1,3)*jcb(3,1))/volume
        jcbi(3,2)=(jcb(1,2)*jcb(3,1)-jcb(1,1)*jcb(3,2))/volume
        jcbi(1,3)=(jcb(1,2)*jcb(2,3)-jcb(1,3)*jcb(2,2))/volume
        jcbi(2,3)=(jcb(1,3)*jcb(2,1)-jcb(1,1)*jcb(2,3))/volume
        jcbi(3,3)=(jcb(1,1)*jcb(2,2)-jcb(1,2)*jcb(2,1))/volume
        jcbip(1,1)=(jcbp(2,2)*jcbp(3,3)-jcbp(2,3)*jcbp(3,2))/volumep
        jcbip(2,1)=(jcbp(2,3)*jcbp(3,1)-jcbp(2,1)*jcbp(3,3))/volumep
        jcbip(3,1)=(jcbp(2,1)*jcbp(3,2)-jcbp(2,2)*jcbp(3,1))/volumep
        jcbip(1,2)=(jcbp(1,3)*jcbp(3,2)-jcbp(1,2)*jcbp(3,3))/volumep
        jcbip(2,2)=(jcbp(1,1)*jcbp(3,3)-jcbp(1,3)*jcbp(3,1))/volumep
        jcbip(3,2)=(jcbp(1,2)*jcbp(3,1)-jcbp(1,1)*jcbp(3,2))/volumep
        jcbip(1,3)=(jcbp(1,2)*jcbp(2,3)-jcbp(1,3)*jcbp(2,2))/volumep
        jcbip(2,3)=(jcbp(1,3)*jcbp(2,1)-jcbp(1,1)*jcbp(2,3))/volumep
        jcbip(3,3)=(jcbp(1,1)*jcbp(2,2)-jcbp(1,2)*jcbp(2,1))/volumep

          do k=1,mpe
            do i=1,3
            b(k,i)=jcbi(i,1)*dhdr(k)+jcbi(i,2)*dhds(k)+jcbi(i,3)*dhdt(k)
            bp(k,i)=jcbip(i,1)*dhdr(k)+jcbip(i,2)*dhds(k)+jcbip(i,3)*dhdt(k)
            enddo
          enddo

         heat=heatproduction

! uncheck the following two lines turn on Brady et al heat production function                        !SK
! if the "if" line is implemented, it will not calculate an equilibrium geotherm for the first step.  !SK 

! heat=12.64*4.0175*exp(-zint/9.36)   !SK Brady et al best fit exponential

!Brady best fit step function                         !SK

 depth=zl-zint                                        !SK
 if (depth.lt.3) heat=2.*12.64*hpt                    !SK
 if (depth.ge.3) heat=(5+depth)/3.5*12.64*hpt         !SK
 if (depth.ge.5) heat=(16.9-depth)/3.8*12.64*hpt      !SK
 if (depth.ge.15) heat=.5*12.64*hpt                   !SK
 if (depth.ge.34) heat=.14*12.64*hpt                  !SK
 if (time.lt..000001) heat=0.                         !SK

                  do j=1,3
                    do i=1,3
                        epsij=0.
                          do k=1,mpe
                          epsij=epsij+b(k,i)*vv(k,j)
                          enddo
                !        stress=10.e6    !Pascals, this is now specified in Pecube !SK
                !        stress=rho*g*(zl-zint)*1.e3*tan(pi/6.)
                        heat=heat+fric*abs(epsij)*stress/rho/c
                     enddo
                  enddo
        call find_velo (xint,yint,zint,dt,velo,time, &
		                xming,xmaxg,yming,ymaxg,zl,vx,vy,vz, &
                                arctrench,ddepth,dip,acr,tacr,Rbuf,az)  !SK

        velox=velo*vx
        veloy=velo*vy
        veloz=velo*vz

		uvnorm=velox**2+veloy**2+veloz**2
        xmin=minval(x)
        xmax=maxval(x)
        dx=xmax-xmin
        ymin=minval(y)
        ymax=maxval(y)
        dy=ymax-ymin
        zmin=minval(z)
        zmax=maxval(z)
        dz=zmax-zmin
		  if (uvnorm.le.tiny(uvnorm)) then
		  tau=0.
		  else
  		  tau=(abs(velox)*dx+abs(veloy)*dy+abs(veloz)*dz)/sqrt(15.)/uvnorm
		  endif

		  do k=1,mpe
		  ht(k)=h(k)+tau*(velox*b(k,1)+veloy*b(k,2)+veloz*b(k,3))
		  enddo

          do j=1,mpe
          bel(j)=bel(j)+h(j)*dt*((1.-alpha)*heat*volumep+alpha*heat*volume)*w
            do i=1,mpe
            xcond=cond*(b(i,1)*b(j,1)+b(i,2)*b(j,2)+b(i,3)*b(j,3))*volume &
                 +rhoc*ht(i)*(velox*b(j,1)+veloy*b(j,2)+veloz*b(j,3))*volume
            xmass=h(i)*rhoc*h(j)*volume*dynamic
            xcondp=cond*(bp(i,1)*bp(j,1)+bp(i,2)*bp(j,2)+bp(i,3)*bp(j,3))*volumep &
                 +rhoc*ht(i)*(velox*bp(j,1)+veloy*bp(j,2)+veloz*bp(j,3))*volumep
            xmassp=h(i)*rhoc*h(j)*volumep*dynamic
            ael(i,j)=ael(i,j)+(xmass+dt*alpha*xcond)*w
            aelp(i,j)=aelp(i,j)+(xmass-dt*(1.-alpha)*xcondp)*w
            enddo
          enddo

        enddo

        do i=1,mpe
          do j=1,mpe
          bel(i)=bel(i)+aelp(i,j)*tempp(icon(j))
          enddo
        enddo

        do i=1,mpe
          if (kfix(icon(i)).eq.1) then
          fixt=tempp(icon(i))
            do j=1,mpe
            bel(j)=bel(j)-ael(j,i)*fixt
            ael(i,j)=0.
            ael(j,i)=0.
            enddo
          ael(i,i)=1.
          bel(i)=fixt
          endif
        enddo

      deallocate (rr,ss,tt,ww)
      deallocate (aelp,b,bp,vv)
      deallocate (x,y,z,xp,yp,zp)
      deallocate (h,ht,dhdr,dhds,dhdt)

      return
     end
