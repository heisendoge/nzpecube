      subroutine find_velo (x,y,z,dt,Peclet,time,&
	                        xmin,xmax,ymin,ymax,zl,vx,vy,vz, &
                                arctrench,ddepth,dip,acr,tacr,Rbuf,az)  !SK

! this extra layer between Pecube and the velocity function geometry ensures
! that the advection of the "lagrangian particles" for which we track the thermal
! history (to calculate the age) is second order accurate; it uses a mid-point
! algorithm.

      implicit real*8 (a-h,o-z)

      xx0=x
	  yy0=y
	  zz0=z
	  xx=xx0
	  yy=yy0
	  zz=zz0

ico=0
1     call geometry (xx,yy,zz,vx,vy,vz,time,xmin,xmax,ymin,ymax,zl,dt, &
                     arctrench,ddepth,dip,acr,tacr,Peclet,Rbuf,az) !SK
      ico=ico+1
      xxn=xx0+dt*Peclet*vx/2.
      yyn=yy0+dt*Peclet*vy/2.
      zzn=zz0+dt*Peclet*vz/2.
      xnorm=sqrt((xxn-xx)**2+(yyn-yy)**2+(zzn-zz)**2)
      xx=xxn
      yy=yyn
      zz=zzn
      if (ico.gt.10) goto 12
      if (xnorm.gt.zl*1.e-6) goto 1

12 continue
      return
  end
