      subroutine geometry (xp,yp,zp,vxp,vyp,vzp,time,xmin,xmax,ymin,ymax,zl,dt, &
                           arctrench,ddepth,dip,acr,tacr,Peclet,Rbuf,az)  !SK
      implicit real*8 (a-h,o-z)

      real*8,dimension(:),allocatable::xi,zi,alphai
      real*8:: vxp,vyp,vzp
      real*8:: Raz,Laz        !SK R & L accretion zone end points
 
      pi=atan(1.)*4.
 
! n is the number of segments along the fault surface

      n=3

      pi=atan(1.)*4.
      con=atan(1.)/45.

      allocate (xi(n),zi(n),alphai(n))

! prescribe the geometry of each segment (sometimes with respect to the previous one)

      zi(1)=0.
      xi(1)=-10.
      alphai(1)=0.

      zi(2)=0.
      xi(2)=Rbuf+zl/(tan((pi-dip)/2.))
      alphai(2)=dip

      zi(3)=ddepth
      xi(3)=xi(2)+(zi(3)-zi(2))/tan(alphai(2))
      alphai(3)=0.

! change of coordinate system

       x=xmax-xp
       y=yp
       z=zl-zp

! reset velocities

      vx=0.
      vy=0.
      vz=0.

! calculates velocities according to algorithm described in notes/manuals
     ra=1.   ! ra is the relative velocities between overlying and underlying plate

      do i=1,n
      
!          if (x.lt.xi(i)) goto 1
!	  if (-(x-xi(i))*sin(alphai(i))+(z-zi(i))*cos(alphai(i)).gt.0.) goto 1
          vgrad=abs(-(x-xi(i))*sin(alphai(i))+(z-zi(i))*cos(alphai(i)))
          if (vgrad.gt.2) then
           vgrad=1.
         else
           vgrad=vgrad/2.
       vgrad=1. !SK
          endif

          if (-(x-xi(i))*sin(alphai(i))+(z-zi(i))*cos(alphai(i)).gt.0.) then
           sx=1.*vgrad*ra
           sz=1.*vgrad*ra
          else
           sx=-1.*vgrad*(1.-ra)
           sz=-1.*vgrad*(1.-ra)
          endif

	  if (i.ne.n .and. (x-xi(i+1))*cos((alphai(i)+alphai(i+1))/2.)+(z-zi(i+1))*sin((alphai(i)+alphai(i+1))/2.).gt.0.) goto 1

          vx=sx*cos(alphai(i))
          vz=sz*sin(alphai(i))

          goto 2

1     continue
      enddo

2     continue

! 'un'change of coordinate system

      vxp=-vx
      vyp=-vy
      vzp=-vz

!SK  introduce a zone of accretion at chosen time             !SK

      Raz= Rbuf + zl/(tan((pi-dip)/2.)) + arctrench - az/2.    !SK subtract 2.1 to get exact match with old classic
      Laz= Rbuf + zl/(tan((pi-dip)/2.)) + arctrench + az/2.    !SK subtract 2.1 to get exact match with old classic

      if (time.gt.tacr) then                                  !SK set time where accretion starts
        if (x.gt.Raz .and.x.lt.Laz) vzp=vzp+(acr/Peclet)    !SK set window where accretion happens (0 at R)
!	 						      !SK fraction = accretion rate as fraction of Pecube (set lower# = Pecube)
!                                                            !SK this keeps vx component constant- may not be appropriate always
      endif                                                   !SK velocity is 1 km per time step


10      deallocate (xi,zi,alphai)

      return

      end
