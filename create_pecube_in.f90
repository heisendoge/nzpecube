      subroutine create_pecube_in (tmax,zl,xl,Peclet,tend,fnx,nz)

! Subroutine to create the input file Pecube.in to be used by Pecube
! In the original version of this file, one uses a small sample of the GTOPO30
! dataset from the Southern Alps (NZ) to create a sample topography
! It is assumed that topography is originaly flat (at zmax) and remains so
! in the period [0,tcarve] the landscape is then carved in the period
! [tcarve,tstop] and remains in steady-state in the period [tstop,tend]
! Note that time increases from past to present (I am a physicist, not a geologist)
  
! The user can modify this file at will but the output it produces (Pecube.in)
! must obey rules that are described in the user guide and in Pecube.f90

      implicit real*8 (a-h,o-z)
      integer :: fnx

      character run*5
      common /nxny/ nx,ny

      run='RUN00'

      nx=fnx   !SK didn't want to mess with common variable so made up fnx
      ny=2
!     nz=31    !SK now carried in from Pecube.f90
!     crustal_thickness=120.e3   !SK now input in Pecube.f90
!     xl=330. !SK now input in Pecube.f90
      yl=1.
!     zl=crustal_thickness/1.e3  !SK now input in Pecube.f90

      open (7,file='Pecube.in',status='unknown')

!      Peclet=100.
!      tend=4.5
       nstep=1

       diffusivity=25.   !SK (units km**2/m.y.) !try 1.e-16 for zero conductivity SK !philpotts suggests 31.6 (p75)
!      tmax=1278.   !SK, now specified in Pecube.f90
       tmsl=0.
       tlapse=0.

     heatproduction=.01*12.64 !uniform heat production option, units of first term are uW/m^3 SK
     heatproduction=0.   !SK check to turn on uniform heat production
 
      ilog=0
      iterative=1
      interpol=1

      write (7,'(a)') run
      write (7,*) 4,nx*ny,nz,(nx-1)*(ny-1),zl,diffusivity,heatproduction
      write (7,*) tmax,tmsl,tlapse,nstep,ilog,iterative,interpol

        do j=1,ny
          do i=1,nx
          x=xl*float(i-1)/float(nx-1)
          y=yl*float(j-1)/float(ny-1)
          write (7,*) x,y
          enddo
        enddo 

        do j=1,ny-1
          do i=1,nx-1
          icon1=(j-1)*nx+i
          icon2=icon1+1
          icon3=icon1+nx+1
          icon4=icon1+nx
          write (7,*) icon1,icon2,icon3,icon4
          enddo
        enddo

! initial conditions

        write (7,*) 0.,0.,1
        write (7,*) (0.,k=1,nx*ny)

! end of 1st period

        write (7,*) tend,Peclet,1
        write (7,*) (0.,k=1,nx*ny)

      close (7)

      return
      end
