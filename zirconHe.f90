      subroutine zirc_he (times,temperature,ntime,zircon_age)

! this routine calculates ages for U-Th/He for zircon
      implicit real (a-h,o-z)
      real times(ntime),temperature(ntime)
      real,dimension(:),allocatable::age,diag,sup,inf,f
      real*4 d0
!the value of the paramters are from Reiners et al. (2003)
      D0a2=(768487.1)*3600.*24.*365.25e6
       D0a2=((.46**.87)*1.e-4/(60.e-6**2))*3600.*24.*365.25e6
!	  print*,D0a2
      Ea=40.4e3*4.184
      R=8.314
      dt0=0.1

      n=100

      allocate (age(n),diag(n),sup(n),inf(n),f(n))

      age=0.

      do itime=1,ntime-1

      dt=dt0
1     nstep=max(1,int((times(itime)-times(itime+1)+tiny(dt))/dt))
      dt=(times(itime)-times(itime+1))/nstep
      alpha=0.5
      dr=1./(n-1)
! beta determines the geometry: 2 = spherical
!                               1 = cylindrical
      beta=2.

      temps=temperature(itime)
      tempf=temperature(itime+1)
if (temps.lt.0.) temps=0.
if (tempf.lt.0.) tempf=0.

      Da2now=D0a2*exp(-Ea/R/(temps+273.))
!print*,log(Da2now)
        do istep=1,nstep

        fstep=float(istep)/float(nstep)
        temp=temps+(tempf-temps)*fstep
        Da2then=Da2now
        Da2now=D0a2*exp(-Ea/R/(temp+273.))
        f1=alpha*dt*Da2now/dr**2
        f2=(1.-alpha)*dt*Da2then/dr**2

          do i=2,n-1
          diag(i)=1.+2.*f1
          sup(i)=-f1*(1.+beta/(i-1)/2.)
          inf(i)=-f1*(1.-beta/(i-1)/2.)
          f(i)=age(i)+f2* &
               ((age(i+1)-2.*age(i)+age(i-1)) + &
                beta*(age(i+1)-age(i-1))/(i-1)/2.)+dt
          enddo

        diag(1)=1.
        sup(1)=-1.
        f(1)=0.
        diag(n)=1.
        inf(n)=0.
        f(n)=0.

        call tridag (inf,diag,sup,f,age,n)

        agei=0.
          do i=1,n
          fact=1.
          if (i.eq.1.or.i.eq.n) fact=0.5
          agei=agei+age(i)*fact*dr**3*(i-1)*(i-1)
          enddo
        agei=3.*agei

        enddo

      enddo

      zircon_age=agei

      return
      end
