      program Pecube

      implicit real*8 (a-h,o-z)

      real*8,dimension(:),allocatable :: x,y,z,xp,yp,zp
      real*8,dimension(:),allocatable :: t,tp,f,tpi
      integer,dimension(:,:),allocatable :: icon
      real*8,dimension(:,:,:),allocatable :: ael
      real*8,dimension(:,:),allocatable :: bel
      integer, dimension(:),allocatable :: kfix,ielsurf
      real*8,dimension(:),allocatable :: xsurf,ysurf,zsurf,zsurfp
      integer,dimension(:,:),allocatable :: iconsurf,neighbour
      real*8,dimension(:),allocatable :: xdepth,ydepth,zdepth,tprev
!     real*8,dimension(:),allocatable :: xtest,ytest,ztest   !SK
!     real*8,dimension(:),allocatable :: xtemp,ytemp,ztemp   !SK
      real*8,dimension(:),allocatable :: xin,yin,zin,tin,tbig,Mage,Hage,Kage     !SK for superracking particles
      real*8,dimension(:),allocatable:: xexhumation,yexhumation,zexhumation
      real*4,dimension(:),allocatable :: t2,t2p,t3,t4,t5,stp_max

      real*4 t1
      integer :: nin, TurSch,fnx                                                            !SK
      real*8 :: hpt, stress, arctrench, ddepth, dip, xl, zl, Peclet, tend, Lbuf, Rbuf, az   !SK
      real*8 :: tmax, ocrustage, concrustage, crustchange, ocrustT, ccrustT, trenchtocc     !SK
      real*4 :: hs, cr, ig, pk, gr, hsw, crw, igw, pkw, grw, hsfit, crfit, igfit, pkfit, grfit, score   !SK      
 
      character run*5
      logical interpol_success

      common /nxny/ nx,ny

      eps=tiny(eps)
      imovie=0
      nin=84   !SK  this is number of points I supertrack, NOTE: if this changes must also change SK computations at end
      pi=atan(1.)*4.

      TurSch=1 !SK turns T&S geotherm on or off, 0=off, 1=on 

!SK These variables don't vary (at least not in NA)

zl=120.                     !model thickness (km)
Lbuf=90.                  !space (km) between where accretion occurs & L model edge, 127.9 standard, 90 OK @ acr=5.
Rbuf=16.                !space (km) between hinge-base intersect. & R model edge, old stand: 27.8461, 16. fine
      
!SK  The following are variables that can vary
 
stress=15784850.       !shear stress (Pa), standard 10.e6, range: 0-100
arctrench=194.441     !distance (km) from trench to where tracked particels accrete, range: 140-250 km
ddepth=28.747          !depth of decollment flat (km), range: 20-40 km
Peclet=120.587         !slip rate on decollment (km/m.y.)
tmax=1300.          !temp at model base (deg C), range: 1000-1400
acr=.585              !accretion rate (km/m.y.), range: .1-10
az=83.557             !width (km, in x direction) of accretion zone
ocrustage=43.516       !T&S age (m.y.) of oceanic crust, vary 4 to 60
concrustage=23.333     !T&S 'age' (m.y.) of 'continental' crust, range:
hpt=.636              !heat production tuning parameter, vary .5 - 1.5 to vary percentage of Brady heat production

! stress=        0.0     
! arctrench=     200.     
! ddepth=        25.     
! Peclet=        100.     
! tmax=          1400.   
! acr=           .5     
! az=            10.     
! ocrustage=     40.    
! concrustage=   6.     
! hpt=           1. 

dip=pi/6.  !decollment dip between trench & flat, must be between atan(ddepth/arctrench) and pi/4. degrees
trenchtocc=50.              !space (km) betwn trench & where arc gthrm strts, old standard:50 km, range:
xl=arctrench+Lbuf+Rbuf+zl/(tan((pi-dip)/2.))                          !width of model
crustchange=xl-Rbuf-zl/(tan((pi-dip)/2.))-trenchtocc                  !distance from L side where geotherm switches
tacr=((ddepth/sin(dip))+arctrench-(ddepth/tan(dip)))/Peclet           !time (m.y.) until accretion starts
tend=tacr+ddepth/acr        !end time of model (m.y.) = time when leading schist particle returns to surface
fnx=xl/7.333*xl/330.       !# nodes in x direction (becomes 'nx' in create_Pecube_in.f90), old standard=7.3333 (km), mini=13.2, good=2.
nz=61                       !# nodes in z direction

!some simple checks on geometry that might find an error          !SK
  if (xl-Rbuf-(zl/tan((pi-dip)/2. ))-arctrench-Lbuf.gt.01) then   !SK
      write(*,*) 'geometry problem'                               !SK
      pause                                                       !SK
  endif                                                           !SK
  if (dip.lt.atan(ddepth/arctrench)) then                         !SK
      write(*,*) 'dip not steep enough'                           !SK
      pause                                                       !SK
  endif                                                           !SK

!option to create matlab readable file 'velo2D' to visualize the geometry        !SK
call velocheck (xl,fnx,nz,zl,dt,arctrench,ddepth,dip,acr,tacr,Peclet,Rbuf,az)    !SK


! Pecube is a Finite Element solver of the 3D, transient heat transfer
! equation that allows for conduction, vertical advection and production of
! heat. Pecube also allows for the surface geometry to vary with time.

      print*,'Reading input...'


! comment this line out if you want Pecube to read your own input file
! (named Pecube.in)

      call create_pecube_in (tmax,zl,xl,Peclet,tend,fnx,nz)

! opens input files

      open (7,file='Pecube.in',status='old')

! read in general information

! first line:
! run: 5 character string that will determine the name of the folder where the input file
!      will be copied to and where the output files (Pecube.ou and Pecube.ptt) will be stored.

      read (7,'(a)') run
      open (17,file=run//'/Pecube.in',status='unknown',err=999)
      goto 998
999   print*,'Error - cannot create files in '//run//' directory...'
      stop
998   write (17,'(a)') run

! second line
! npe: number of nodes per surface (2D) elements (3 = triangular elements - 4 = rectangular elements)
! nsurf: number of nodes defining the surface geometry
! nz: number of nodes in the vertical direction
! nelemsurf: number of surface 2D elements
! zl: thickness of crustal layer (= depth at which t is fixed) (in km)
! diffusivity: thermal diffusivity (in km^2/Myr)
! heatproduction: heat production (in degC/Myr)

      read (7,*) npe,nsurf,nz,nelemsurf,zl,diffusivity,heatproduction
      write (17,*) npe,nsurf,nz,nelemsurf,zl,diffusivity,heatproduction
      mpe=2*npe
      allocate (iconsurf(npe,nelemsurf),ielsurf(nsurf),neighbour(npe,nelemsurf))
      allocate (xsurf(nsurf),ysurf(nsurf),zsurf(nsurf),zsurfp(nsurf))
      allocate (xdepth(nsurf),ydepth(nsurf),zdepth(nsurf),tprev(nsurf))
!     allocate (xtest(nsurf),ytest(nsurf),ztest(nsurf))   !SK
      allocate (xin(nin),yin(nin),zin(nin),tin(nin),tbig(nin),Mage(nin),Hage(nin),Kage(nin))               !SK

	  allocate (xexhumation(nsurf),yexhumation(nsurf),zexhumation(nsurf))

! second line:
! tmax: basal temperature (at z=-zl) (in degC)
! tmsl: temperature at mean sea level (z=0) (in degC)
! tlapse: lapse rate (indegC/km) 
! nstep: number of stages (the surface topo and exhumatoin rate can be specified at each time stage)
! ilog: redundant (dont use)
! iterative: iterative solver method (1 = Gauss Siedel with overrelaxation - 2 = Conjugate gradient)
! interpol: interpolation used (1 = linear - 2 = quadratic, not recommended)

      read (7,*) tmax,tmsl,tlapse,nstep,ilog,iterative,interpol
      write (17,*) tmax,tmsl,tlapse,nstep,ilog,iterative,interpol

      if (ilog.eq.1) open (9,file='Pecube.log',status='unknown')

! read in nodal geometry

! xsurf, ysurf: x- and y-locations of the surface nodes
! iconsurf: connectivity matrix describing the 2D surface elements

      read (7,*) (xsurf(i),ysurf(i),i=1,nsurf)
      write (17,*) (xsurf(i),ysurf(i),i=1,nsurf)
      read (7,*) ((iconsurf(i,j),i=1,npe),j=1,nelemsurf)
      write (17,*) ((iconsurf(i,j),i=1,npe),j=1,nelemsurf)

      print*,'Preprocessing...'

      xmin=minval(xsurf)
      xmax=maxval(xsurf)
      ymin=minval(ysurf)
      ymax=maxval(ysurf)

! go through the input file a first time to determine the depth of the
! points that will end up at the surface at the end of the model run

      niteradvec=10
      xexhumation=xsurf
      yexhumation=ysurf
      zexhumation=zl
      open(73,file='STparticlelocations',status='unknown') !FH added this to fix the particle problems

	    do istep=nstep,1,-1
        timesurfp=0.
		rewind (7)
	    read (7,*)
	    read (7,*) i1,i2,i3,i4,x1,x2,x3
	    read (7,*) x1,x2,x3,i1,i2,i3,i4
	    read (7,*) (xsurf(i),ysurf(i),i=1,nsurf)
	    read (7,*) ((iconsurf(i,j),i=1,npe),j=1,nelemsurf)
          do kstep=0,istep-1
          read (7,*) timesurfp,Peclet,iout
          if (istep.eq.nstep) write (17,*) timesurfp,Peclet,iout
          read (7,*) (zsurf(i),i=1,nsurf)
          if (istep.eq.nstep) write (17,*) (zsurf(i),i=1,nsurf)
		  enddo
        read (7,*) timesurf,Peclet,iout
        if (istep.eq.nstep) write (17,*) timesurf,Peclet,iout
        read (7,*) (zsurf(i),i=1,nsurf)
        if (istep.eq.nstep) write (17,*) (zsurf(i),i=1,nsurf)
        if (istep.eq.nstep) zexhumation=zexhumation+zsurf
        if (istep.eq.nstep) tfinal=timesurf
		call find_dt (zl,diffusivity,nsurf,zsurf,zsurfp, &
		              nz,Peclet,timesurf,timesurfp,istep,eps,ilog, &
					  dt,ntime,istatic)

!SK these are 'supertracked' particle endpoints for which beginning points will be calculated.

zin(1)=114.
zin(2)=116.
zin(3)=117.
zin(4)=118.
zin(5)=119.
zin(6)=119.5
zin(7)=119.8
zin(8)=121.
zin(9)=122.
zin(10)=128.
zin(11)=130.
zin(12)=123.5
zin(13)=119.35
zin(14)=130.
zin(15)=129.8
zin(16)=129.6
zin(17)=129.4
zin(18)=129.2
zin(19)=129.
zin(20)=128.8
zin(21)=128.6
zin(22)=128.4
zin(23)=128.2
zin(24)=128.
zin(25)=127.8
zin(26)=127.6
zin(27)=127.4
zin(28)=127.2
zin(29)=127.
zin(30)=126.8
zin(31)=126.6
zin(32)=126.4
zin(33)=126.2
zin(34)=126.
zin(35)=125.8
zin(36)=125.6
zin(37)=125.4
zin(38)=125.2
zin(39)=125.
zin(40)=124.8
zin(41)=124.6
zin(42)=124.4
zin(43)=124.2
zin(44)=124.
zin(45)=123.8
zin(46)=123.6
zin(47)=123.4
zin(48)=123.2
zin(49)=123.
zin(50)=122.8
zin(51)=122.6
zin(52)=122.4
zin(53)=122.2
zin(54)=122.
zin(55)=121.8
zin(56)=121.6
zin(57)=121.4
zin(58)=121.2
zin(59)=121.
zin(60)=120.8
zin(61)=120.6
zin(62)=120.4
zin(63)=120.2
zin(64)=120.
zin(65)=119.8
zin(66)=119.6
zin(67)=119.4
zin(68)=119.2
zin(69)=119.
zin(70)=118.8
zin(71)=118.6
zin(72)=118.4
zin(73)=118.2
zin(74)=118.
zin(75)=117.8
zin(76)=117.6
zin(77)=117.4
zin(78)=117.2
zin(79)=117.
zin(80)=116.8
zin(81)=116.6
zin(82)=116.4
zin(83)=116.2
zin(84)=116.

   do i=1,nin
      yin(i)=0.0
      xin(i)=Lbuf
   enddo

! this section finds initial positions and records the path from final to initial position !SK
! for both super-tracked and surface-seeking particles
 
           !FH adds these 4 lines so that the particle positions files has the final position in it
            write(73,*) tfinal, nin,ntime*nstep
print*,'ntime=',ntime
            do i=1,nin
                        write(73,*) xin(i),zin(i) !,tin(i)
            enddo


          do ktime=1,ntime
          time=timesurf-dt*ktime
            do i=1,nsurf
			xxx=xexhumation(i)
			yyy=yexhumation(i)
			zzz=zexhumation(i)
			  do k=1,niteradvec
  		      call find_velo (xxx,yyy,zzz,dt,Peclet, &
			                  time,xmin,xmax,ymin,ymax,zl,vx,vy,vz, &
                                          arctrench,ddepth,dip,acr,tacr,Rbuf,az)  !SK
		      dxxx=xexhumation(i)-dt*Peclet*vx/2.-xxx
		      dyyy=yexhumation(i)-dt*Peclet*vy/2.-yyy
		      dzzz=zexhumation(i)-dt*Peclet*vz/2.-zzz
			  xxx=xxx+dxxx
			  yyy=yyy+dyyy
			  zzz=zzz+dzzz
			  enddo
             xexhumation(i)=xexhumation(i)-dt*Peclet*vx
             yexhumation(i)=yexhumation(i)-dt*Peclet*vy
             zexhumation(i)=zexhumation(i)-dt*Peclet*vz
            enddo

write(73,*) time, nin,ntime*nstep 
            do i=1,nin
                        xxx=xin(i)
                        yyy=yin(i)
                        zzz=zin(i)
                  do k=1,niteradvec   !SK line and 7 marked below are optional, see 070614&15 folder
                      call find_velo (xxx,yyy,zzz,dt,Peclet, &
                                          time,xmin,xmax,ymin,ymax,zl,vx,vy,vz, &
                                          arctrench,ddepth,dip,acr,tacr,Rbuf,az)  !SK

		      dxxx=xin(i)-dt*Peclet*vx/2.-xxx   !optional SK
		      dyyy=yin(i)-dt*Peclet*vy/2.-yyy   !optional SK
		      dzzz=zin(i)-dt*Peclet*vz/2.-zzz   !optional SK
			  xxx=xxx+dxxx   !optional SK
			  yyy=yyy+dyyy   !optional SK
			  zzz=zzz+dzzz   !optional SK
  	                  enddo          !optional SK

             xin(i)=xin(i)-dt*Peclet*vx
             yin(i)=yin(i)-dt*Peclet*vy
             zin(i)=zin(i)-dt*Peclet*vz
             write(73,*) xin(i),zin(i) !,tin(i)
            enddo

         enddo
		zsurfp=zsurf
		timesurfp=timesurf
		enddo

! _depth is the initial location of the rocks that end up at the location of the
! surface nodes at the end of the model experiment

          xdepth=xexhumation
	  ydepth=yexhumation 
	  zdepth=zexhumation


!AAA SK inserts this to track temperature change of particles headed down 
!this bit creates a file i can alter later and use as input SK
! NOTE: this can not be on at same time as next bit (AAB) SK
!  open (65,file='xyzinput.txt',status='unknown') !SK
! do i=1,nsurf   !SK
! write (65,*) xdepth(i), ydepth(i), zdepth(i)  !SK
! enddo          !SK

!AAB this bit (combined with creating & allocating variables) reads an input file SK
! containing the locations of particles headed downward  SK
!open (65,file='xyzinput.txt',status='unknown') !SK
!do i=1,nsurf  !SK
!read (65,*) xtest(i),ytest(i),ztest(i)   !SK
!enddo        !SK 
!xdepth=xtest !SK
!ydepth=ytest !SK
!zdepth=ztest !SK


! reset the input file to its proper position

      rewind (7)
      read (7,'(a)') run
      read (7,*) npe,nsurf,nz,nelemsurf,zl,diffusivity,heatproduction
      read (7,*) tmax,tmsl,tlapse,nstep,ilog,iterative,interpol
      read (7,*) (xsurf(i),ysurf(i),i=1,nsurf)
      read (7,*) ((iconsurf(i,j),i=1,npe),j=1,nelemsurf)

      nnode=nsurf*nz
      nelem=nelemsurf*(nz-1)
      if (ilog.eq.1) write (9,*) 'nnode/nelem= ',nnode,nelem

! opens output files

! Pecube.out contains the temperature field at the end of each stage

      open (8,file=run//'/Pecube.out',status='unknown',access='direct', &
            recl=4*nnode)
! Pecube.ptt contains the depth-temperture-paths of all surface nodes

      open (10,file=run//'/Pecube.ptt',status='unknown',access='direct', &
            recl=4*(1+nsurf*4))

      allocate (x(nnode),y(nnode),z(nnode),t(nnode))
      allocate (xp(nnode),yp(nnode),zp(nnode),tp(nnode),tpi(nnode))
      allocate (icon(mpe,nelem),ael(mpe,mpe,nelem),bel(mpe,nelem))
      allocate (kfix(nnode))
      allocate (f(nnode))

! build 3D element connectivity

      ie=0
        do iesurf=1,nelemsurf
          do k=1,(nz-1)
          ie=ie+1
            do kk=1,npe
            icon(kk,ie)=(iconsurf(kk,iesurf)-1)*nz+k
            icon(kk+npe,ie)=icon(kk,ie)+1
            enddo
          enddo
        enddo

      if (ie.ne.nelem) stop 'nelem mismatch'

      if (ilog.eq.1) then
      write (9,*) 'icon'
        do ie=1,nelem
        write (9,*) (icon(k,ie),k=1,mpe)
        enddo
      endif

! finds neighbour conectivity matrix

      call find_neighbours (iconsurf,neighbour,npe,nelemsurf,nsurf)
      ielsurf=1

! initialize global parameters
! alpha is the time integration parameter

      alpha=0.5
      time=0.
      timesurf=0.
      irec=0
      jrec=0

      print*,'        Input read and preprocessing performed'
	  print*,'*****************Start of computations ******************'
! begining of surface stepping (stageing)

      do istep=0,nstep

      if (ilog.eq.1) write (9,*) 'istep= ',istep

      if (istep.ne.0) zsurfp=zsurf
      timesurfp=timesurf

! read the step information

! timesurf is the time since the begining of the experiment
! Peclet is the exhumation velocity since the end of the last stage
! iout indicates if the T information is to be saved at the end of the stage

      read (7,*) timesurf,Peclet,iout
        if (istep.eq.0) then
          if (timesurf.gt.eps) then
          print*,'timesurf= ',timesurf
          stop 'first topography record must be at time zero ...'
          endif
        endif
      read (7,*) (zsurf(i),i=1,nsurf)

! initial (or zeroth) step

        if (istep.eq.0) then
        zsurfp=zsurf

        zsurfmax=maxval(zsurf)
        in=0
          do i=1,nsurf
            do k=1,nz
            in=in+1
            fact=float(k-1)/float(nz-1)
            if (interpol.eq.2.and.fact.gt.eps) fact=sqrt(fact)
            zh=zsurf(i)+zl
            zp(in)=zh*fact
            xp(in)=xsurf(i)
            x(in)=xp(in) 
            yp(in)=ysurf(i)
            y(in)=yp(in)
            kfix(in)=0
            if (k.eq.1 .or. k.eq.nz) kfix(in)=1
            enddo
          enddo

! calculates initial temperature

          do i=1,nsurf
          tsurf=-zsurf(i)*tlapse+tmsl
            do k=1,nz
            in=(i-1)*nz+k
            zh=zsurf(i)+zl
            tp(in)=tsurf+(tmax-tsurf)*(zh-zp(in))/zh
            enddo
          enddo

        t=tp
 
! (1of2): The following can be un!'d to create a geothermal gradient following Turcotte&Schubert eq.4-125, !SK
! or two T&S geotherms (one for arc, one for oceanic crust).                                               !SK

  if (TurSch.eq.1) then
      ocrustT=tmax/(erf(zl*1000000/(2*sqrt(60.*60.*24.*365.25*1000000.*ocrustage)))) !SK sets T @ infinity so tmax is right @ crust base
      ccrustT=tmax/(erf(zl*1000000/(2*sqrt(60.*60.*24.*365.25*1000000.*concrustage)))) !SK sets T @ infinity so tmax is right @ crust base

         trenchnode=nint(crustchange/xl*nx) !SK this picks the nearest node to where crust change should be
        do i=1,trenchnode          !SK
            do k=1,nz               !SK
              in=(i-1)*nz+k         !SK
              zh=zsurf(i)+zl        !SK
              tp(in)=ccrustT*erf((zh-(zh/(nz-1)*(k-1)))*1000000./(2.*sqrt(60.*60.*24.*365.25*1000000.*concrustage)))   !SK
             !tp(in)=tsurf+(ccrustT-tsurf)*(zh-zp(in))/zh   !SK linear option 
           enddo  !SK
          enddo !SK
          do i=trenchnode+1,nx      !SK!
            do k=1,nz               !SK
              in=(i-1)*nz+k         !SK
              zh=zsurf(i)+zl        !SK
              tp(in)=ocrustT*erf((zh-(zh/(nz-1)*(k-1)))*1000000./(2.*sqrt(60.*60.*24.*365.25*1000000.*ocrustage)))   !SK
            enddo  !SK
          enddo  !SK
           do i=nx+1,nx+1+trenchnode !SK            
             do k=1,nz               !SK
               in=(i-1)*nz+k         !SK
              zh=zsurf(i)+zl        !SK
             tp(in)=ccrustT*erf((zh-(zh/(nz-1)*(k-1)))*1000000./(2.*sqrt(60.*60.*24.*365.25*1000000.*concrustage)))   !SK
           enddo  !SK
          enddo !SK
          do i=nx+1+trenchnode,nx*2 !SK
            do k=1,nz               !SK
              in=(i-1)*nz+k         !SK
              zh=zsurf(i)+zl        !SK
              tp(in)=ocrustT*erf((zh-(zh/(nz-1)*(k-1)))*1000000./(2.*sqrt(60.*60.*24.*365.25*1000000.*ocrustage)))   !SK
            enddo  !SK
          enddo  !SK
      tpi=tp  !SK this preserves the initial temperature distribution so it can be reinserted later
  end if

          if (ilog.eq.1) then
          write (9,*) 'Nodal geometry'
            do i=1,nnode
            write (9,*) i,xp(i),yp(i),zp(i),tp(i),kfix(i)
            enddo
          endif

        endif

		call find_dt (zl,diffusivity,nsurf,zsurf,zsurfp, &
		              nz,Peclet,timesurf,timesurfp,istep,eps,ilog, &
					  dt,ntime,istatic)

! beginning of time stepping

        do itime=1,ntime
        time=time+dt
        ftime=float(itime)/float(ntime)
        ftimep=float(itime-1)/float(ntime)

        if (ilog.eq.1) write (9,*) 'itime,time,Pe= ',itime,time,Peclet

! build new node geometry

        z=zp
          do i=1,nsurf
          in=i*nz
          inp=i*nz-1
          zsurf1=zsurfp(i)+(zsurf(i)-zsurfp(i))*ftime+zl
          zsurf2=zsurfp(i)+(zsurf(i)-zsurfp(i))*ftimep+zl
          z(in)=z(in)+zsurf1-zsurf2
          z(inp)=z(inp)+(zsurf1-zsurf2)/2.
          enddo      

        if (in.ne.nnode) stop 'nnode mismatch'

! build local FE matrices

          do ie=1,nelem
          call make_matrix (mpe,ael(1,1,ie),bel(1,ie),icon(1,ie),x,y,z,xp,yp,zp, &
                            kfix,diffusivity,heatproduction,Peclet, &
                            alpha,dt,time,tp,nnode,istatic,zl, &
                            xmin,xmax,ymin,ymax,stress,hpt, &
                            arctrench,ddepth,dip,acr,tacr,Rbuf,az)   !SK
          enddo

! build global RHS vector

        f=0.
          do ie=1,nelem
            do k=1,mpe
            ic=icon(k,ie)
            f(ic)=f(ic)+bel(k,ie)
            enddo
          enddo

! solve global FE equations

        if (iterative.eq.1) then
        call solve_iterative (mpe,1,ael,f,t,kfix,icon,nnode,nelem,niter)
        if (ilog.eq.1) write (9,*) niter,' iterations'
        elseif (iterative.eq.2) then
        call solve_conjugate_gradient (mpe,1,ael,f,t,kfix,icon,nnode,nelem,niter)
        if (ilog.eq.1) write (9,*) niter,' iterations'
        else
        stop 'solution strategy not implemented'
        endif


!T&S(2of2) SK added this bit to reinsert T&S geotherm into first step which is destroyed in solve_iterative
 if (TurSch.eq.1) then
   if (istep.eq.0) then                                !SK
      t=tpi                                            !SK
   endif                                               !SK
 end if

! stretch old grid

          do i=1,nsurf
            do k=1,nz
            in=(i-1)*nz+k
            fact=float(k-1)/float(nz-1)
            if (interpol.eq.2.and.fact.gt.eps) fact=sqrt(fact)
            zsurf1=zsurfp(i)+(zsurf(i)-zsurfp(i))*ftime
            zh=zsurf1+zl
            zp(in)=zh*fact
            enddo
          enddo

! interpolate result onto undeformed mesh

          do i=1,nsurf
          ij=(i-1)*nz+1
          call interpolate (t(ij),tp(ij),z(ij),zp(ij),nz)
          enddo


          do i=1,nsurf

! this is part of the standard way of tracking particles !SK
		  xxx=xdepth(i)
		  yyy=ydepth(i)
		  zzz=zdepth(i)
		    do k=1,niteradvec
 		    call find_velo (xxx,yyy,zzz,dt,Peclet, &
		                   time,xmin,xmax,ymin,ymax,zl,vx,vy,vz, &
                                   arctrench,ddepth,dip,acr,tacr,Rbuf,az) !SK
		    dxxx=xdepth(i)+dt*Peclet*vx/2.-xxx
		    dyyy=ydepth(i)+dt*Peclet*vy/2.-yyy
		    dzzz=zdepth(i)+dt*Peclet*vz/2.-zzz
		    xxx=xxx+dxxx
		    yyy=yyy+dyyy
		    zzz=zzz+dzzz
		    enddo
		  xdepth(i)=xdepth(i)+Peclet*vx*dt
          ydepth(i)=ydepth(i)+Peclet*vy*dt
          zdepth(i)=zdepth(i)+Peclet*vz*dt


          call find_element (xdepth(i),ydepth(i),zdepth(i),tnow,x,y,z,t, &
                             xsurf,ysurf,zsurf,ielsurf(i),neighbour, &
                             iconsurf,icon,nelemsurf,nelem,nsurf,nz,nnode,npe,mpe, &
                             interpol_success)
            if (interpol_success) then
            tprev(i)=tnow
            else
            tprev(i)=-tmax
            endif
          enddo

! update ptt for supertracked particles SK
          rewind(73)
          xxtime=1.e6
          do while(xxtime.ge.time)
            read(73,*) xxtime,nnn,xjunk
             do ikl=1,nnn
               read(73,*) xin(ikl),zin(ikl) !,tin(ikl)
             enddo
             if (xxtime-1.e-3.lt.time) goto 153
          enddo
          153 continue

          do i=1,nin
          call find_element (xin(i),yin(i),zin(i),tnow,x,y,z,t, &
                             xsurf,ysurf,zsurf,ielsurf(i),neighbour, &
                             iconsurf,icon,nelemsurf,nelem,nsurf,nz,nnode,npe,mpe, &
                             interpol_success)
            if (interpol_success) then
            tin(i)=tnow
            else
            tin(i)=-tmax
            endif
	  enddo

! saves tt and pt

          jrec=jrec+1
          write (10,rec=jrec) sngl(tfinal-time),sngl(tprev), &
                sngl(zl+zsurfp+(zsurf-zsurfp)*ftime-zdepth),sngl(xdepth),sngl(ydepth)

! this saves XZTt information for tracked particles !SK
!   open (64,file='XZTt.rtf',status='unknown') !SK
!   write(64,*) 'time=', time, nsurf,ntime*nstep  !SK
!   do i=1,nsurf   !SK
!     write (64,*) xdepth(i), zdepth(i), tprev(i)  !SK
!   enddo  !SK

! this saves XZTt information for super-tracked particles !SK
   open (66,file='superXZT.rtf',status='unknown') !SK
   write(66,*) 'time=', time, nin,ntime*nstep  !SK
    do i=1,nin   !SK
      write (66,*) xin(i), zin(i), tin(i)  !SK
    enddo  !SK

!SK this saves the x & z positions, temps, ages of passing through 300 and 525 isotherms and max T's experienced for 
! super-tracked particle  !SK   
    open (68,file='ages_and_temps.rtf',status='unknown')  !SK
    write(68,*) 'time=', time, nin,ntime*nstep  !SK
    write(68,*) 'x        z        temp     300age   525age     Tmax'
     do i=1,nin   !SK
       if (tin(i).gt.300) then   !SK
          Mage(i)=time           !SK
          else                   !SK
       endif                     !SK
       if (tin(i).gt.525) then   !SK
          Hage(i)=time           !SK
          else                   !SK
       endif                     !SK
       if (tin(i).gt.150) then   !SK
          Kage(i)=time           !SK
          else                   !SK
       endif                     !SK
       if (tin(i).gt.tbig(i)) tbig(i)=tin(i)
       write(68,900) xin(i),zin(i),tin(i),Mage(i),Hage(i),tbig(i)   !SK
       900 format (f7.2,f9.2,f9.2,f9.2,f9.2,f9.2)   !SK
     enddo                       !SK

! end of time stepping

        write (*,'(a,i4,a,i4,a,i3,a,i3,a,i6,a)') &
           'Time step ',itime,' of ',ntime, ' in stage ',istep,' of ',nstep,' (',niter,')'

! save file for temperature movie
        if (imovie.eq.0) then
         imovie=1
         open (88,file='temperatureSection.txt',status='unknown')
        endif 
        write(88,*) 'TIME=',time,nx*nz
        do k=1,nx*nz
         write (88,*) x(k),z(k),t(k) 
        enddo

!here we calculate hottest schist T (hs) experienced by the particles, inverted gradient (ig) and cooling rate (cr) !SK

   if (itime.gt.1.and.itime.eq.ntime) then
       print*,'********************  SK Calculations  *********************'

!SK NEW SCHEME

zr=0.
if ((1.1/acr)+5.4-Mage(12).lt.0) zr=(1.1/acr)+5.4-Mage(12) 
!SK youngest zircon time penalty

! = time difference between youngest possible zircon model age in 1.1 km pile (1.1/acr) and
! upper plate Muscovite cooling age (the 5.4 is the slack allowed by difference between youngest zirc
! age =68+/-2 and U.P. cooling at 62.6 Ma OR look at it this way:
! i.e. Mage(12) (62.6Ma) should occur within 5.4 m.y. of deposition of 68 Ma zirc (that makes it into 1.1 km section)
! e.g. acr=1, then 68 Ma occurred by 1.1 model time, and Mage(12) should be lt. 6.5

hs=150./(Kage(11)-Mage(11))   !SK cooling rate 300-150 @ +10 km
cr=200./(Mage(12)-Hage(12))   !SK cooling rate 500-300 @ +3.5 km
ig=Mage(8)-Mage(10)           !SK cooling time lag (~300deg) from +7 km to +1km
pk=tbig(7)                    !SK hottest schist (peak T of uppermost schist particle, degC
gr=(tbig(7)-tbig(13))/.45     !SK inverted gradient in upper 650-200 meters of schist (deg/km)
zrw=10.                       !SK weighting factor for zirc
hsw=2.                        !SK weighting factor for hs, roughly the error bar on data
crw=2.                        !SK weighting factor for cr
igw=1.                        !SK weighting factor for ig
pkw=50.                       !SK weighting factor for pk
grw=40.                       !SK weighting factor for gr
zrfit=(zr/zrw)
hsfit=(hs-15.)/hsw
crfit=(cr-14.5)/crw
igfit=(ig-6.9)/igw
pkfit=(pk-587.)/pkw
grfit=(gr-240.)/grw

score=(zrfit**2.+hsfit**2.+crfit**2.+igfit**2.+pkfit**2.+grfit**2.)**(1./2.)

     write(*,901) zr,zrw,zrfit
     901 format ('zr = ', f9.1, '   zr/', f5.1, ' =', f7.2)
     write(*,902) hs,hsw,hsfit
     902 format ('hs = ', f9.1, '   hs/', f5.1, ' =', f7.2)
     write(*,903) cr,crw,crfit
     903 format ('cooling rate 500-300 @7.0 = ', f9.1, '  (cr-95)/', f5.1, ' = ', f7.2)
     write(*,904) ig,igw,igfit
     904 format ('cooling lag from +7 to -2 = ', f9.1, '   (ig-5)/', f5.1, ' =', f7.2)
     write(*,905) pk,pkw,pkfit
     905 format ('peak temperature @ 200m  = ', f9.1, '  (pk-714.)/', f5.1, ' = ', f7.2)
     write(*,906) gr,grw,grfit
     906 format ('inv gradient              = ', f9.1, '   (gr-100.)/', f5.1, ' =', f7.2)

     write(*,907) score
     907 format ('score=               ', f9.1)
     print*, ''

!SK OLD SCHEME
!hs=tbig(7)                    !SK hottest schist (peak T of uppermost schist particle, degC
!cr=225./(Mage(8)-Hage(8))     !SK cooling rate (525-300degC) from upper plate 1 km above schist (deg/m.y.)
!ig=(tbig(7)-tbig(6))/.3       !SK inverted gradient in upper 300 meters of schist (deg/km)
!hsw=50.                       !SK weighting factor for hs, roughly the error bar on data
!crw=20.                       !SK weighting factor for cr
!igw=40.                       !SK weighting factor for ig
!
!hsfit=(hs-700.)/hsw
!crfit=(cr-80.)/crw
!igfit=(ig-200.)/igw
!score=(hsfit**2.+crfit**2.+igfit**2.)**(1./2.)
!
!     write(*,901) hs,hsw,hsfit
!     901 format ('hottest schist (hs)= ', f9.1, '    (hs-700.)/', f5.1, ' =', f7.2)
!     write(*,902) cr,crw,crfit
!     902 format ('cooling rate (cr)=   ', f9.1, '    (cr-80.)/', f5.1, ' = ', f7.2)
!     write(*,903) ig,igw,igfit
!     903 format ('inverted grad (ig)=  ', f9.1, '    (ig-200.)/', f5.1, ' =', f7.2)
!     write(*,904) score
!     904 format ('score=               ', f9.1)
!     print*, ''

   endif

      enddo

! write output

        if (iout.eq.1) then
        print*,'Saving at step ',istep
        irec=irec+1
        write (8,rec=irec) sngl(zp)
        irec=irec+1
        write (8,rec=irec) sngl(tp)
        endif

! end of surface stepping

      enddo

	  print*,'******************End of computations *******************'

! clean up

      allocate(t2(nsurf),t2p(nsurf),t3(nsurf),t4(nsurf),t5(nsurf))
      t2p=0.
        do krec=jrec,1,-1
        read (10,rec=krec) t1,t2,t3,t4,t5
          do i=1,nsurf
          if (t2(i).lt.0.) t2(i)=t2p(i)
          t2p(i)=t2(i)
          enddo
        write (10,rec=krec) t1,t2,t3,t4,t5
        enddo
      deallocate (t2,t2p,t3,t4,t5)

      jrec=jrec+1
      write (10,rec=jrec) sngl(-1.d0),sngl(tprev), &
           sngl(zl+zsurfp+(zsurf-zsurfp)*ftime-zdepth),sngl(xdepth),sngl(ydepth)

!SK trying to find error in NA version
do i=1,nsurf
  zsurf(i)=0.
  zsurfp(i)=0.
enddo


!SK print*,'*****************Ages calculation*******************'
!SK   call pecubeages(xfit,xsurf,ysurf,zsurf,nsurf)

      close (7)
      close (17)
      close (8)
      if (ilog.eq.1) close (9)
      close (10)

! this bit is adhoc to print a vertical profile to be plotted/contoured
!      open (88,file='temperatureSection.txt',status='unknown')
!        do k=1,31*61
!        write (88,*) x(k),z(k),t(k)
!		enddo
!	  close (88)

      deallocate (iconsurf,ielsurf,neighbour)
      deallocate (xsurf,ysurf,zsurf,zsurfp)
      deallocate (xdepth,ydepth,zdepth,tprev,xexhumation,yexhumation,zexhumation)
      deallocate (xin,yin,zin,tin,tbig,Mage,Hage,Kage) !SK

      deallocate (x,y,z,t)
      deallocate (xp,yp,zp,tp)
      deallocate (icon,ael,bel)
      deallocate (kfix)
      deallocate (f)

      end
