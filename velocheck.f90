!program velocheck

subroutine velocheck (xl,fnx,nz,zl, &
                 dt,arctrench,ddepth,dip,acr,tacr,Peclet,Rbuf,az)    !SK

implicit real*8 (a-h,o-z)
integer :: fnx

xmax=xl
xmin=0.
!nx=25
dx=(xmax-xmin)/float(fnx)
ymax=0.
ymin=0.
!nz=31
!zl=120.
dz=zl/float(nz-1)
! print*,dx,dz,fnx,nz

open(711,file='velo2D.txt',status='unknown')

do i=1,fnx
 do k=1,nz
  xx=float(i-1)*dx
  yy=0.
  zz=float(k-1)*dz
! print*,xx,zz
  call geometry (xx,yy,zz,vx,vy,vz,time,xmin,xmax,ymin,ymax,zl, &
                 dt,arctrench,ddepth,dip,acr,tacr,Peclet,Rbuf,az)    !SK
  write(711,901) xx,zz,vx,vz
  901 format (f10.4,f10.4,f10.4,f10.4)
 enddo
enddo

close(711)

 return          !SK
end
