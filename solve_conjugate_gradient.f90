      subroutine solve_conjugate_gradient (n,ndof,ael,f,x,kfix,icon, &
                                           nnode,nelem,niter)

      implicit real*8 (a-h,o-z)

! conjugate gradient solver

! note that this routine has only been checked for ndof=1

      real*8 ael(n*ndof,n*ndof,nelem),f(ndof*nnode),x(ndof*nnode)
      integer icon(n,nelem),kfix(nnode*ndof)

      real*8,dimension(:),allocatable::r,p,z,ap,rp,zp,diag

! prepare inverse connectivity arrays

      allocate (r(nnode*ndof),p(nnode*ndof),z(nnode*ndof),ap(nnode*ndof))
      allocate (rp(nnode*ndof),zp(nnode*ndof),diag(nnode*ndof))

! prepare diagonal

      diag=0.
        do ie=1,nelem
          do k=1,n
          ic=icon(k,ie)
          diag(ic)=diag(ic)+ael(k,k,ie)
          enddo
        enddo

! calculate reference

      sum=0.
        do inode=1,nnode
        sum=sum+f(inode)**2
        enddo

! start of iterations

      itmax=100000
      tol=1.e-6
      rp=f
      p=rp/diag
      zp=p
      where (kfix.ne.1) x=0.

      it=0
1     it=it+1
      if (it.gt.itmax) stop 'too many iterations'
      ap=0.
        do ie=1,nelem
          do k1=1,n
          i1=icon(k1,ie)
            do k2=1,n
            i2=icon(k2,ie)
            ap(i1)=ap(i1)+ael(k1,k2,ie)*p(i2)
            enddo
          enddo
        enddo

      alpha=0.
      den=0.
        do i=1,nnode
        alpha=alpha+rp(i)*zp(i)
        den=den+p(i)*ap(i)
        enddo
      alpha=alpha/den

      where (kfix.ne.1) x=x+alpha*p

      r=rp-alpha*ap

! check for convergence

      sum0=0.
        do inode=1,nnode
        sum0=sum0+r(inode)**2
        enddo

      if(sum0.lt.sum*tol) goto 2

      z=r/diag
      beta=0.
      den=0.
        do i=1,nnode
        beta=beta+r(i)*z(i)
        den=den+rp(i)*zp(i)
        enddo
      beta=beta/den

      p=z+beta*p
      rp=r
      zp=z

      goto 1

2     continue

      deallocate (r,p,z,ap,rp,zp,diag)

      niter=it

      return
     end
